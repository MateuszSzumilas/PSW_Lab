/*
 * LCD_utils.c
 *
 *  Created on: 27.02.2018
 *      Author: M. Szumilas
 */


#include "LCD_utils.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ti/grlib/grlib.h>

static char* term_buf = NULL;
static uint32_t term_buf_size = 0;
static uint8_t lcd_lines = 0, lcd_cols = 0;
static uint8_t line_spacing = 0;
static uint8_t first_line = 0, current_line = 0;
static Graphics_Context *g_ctx = NULL;

/*
 * prints the buffered string line by line
 */
void _LCD_term_print_buf (void)
{
    Graphics_setForegroundColor(g_ctx, GRAPHICS_COLOR_WHITE);
    Graphics_setBackgroundColor(g_ctx, GRAPHICS_COLOR_BLACK);
    Graphics_setFont(g_ctx, &g_sFontFixed6x8);
    Graphics_clearDisplay(g_ctx);

    uint8_t i;

    for (i=0; i<current_line; i++)
    {
        if (first_line + i < lcd_lines)
            Graphics_drawString(g_ctx,
                                (int8_t *) (term_buf + (first_line + i)*(lcd_cols + 1)),
                                AUTO_STRING_LENGTH,
                                LCD_TERM_MARGIN,
                                LCD_TERM_MARGIN + i*line_spacing,
                                OPAQUE_TEXT);
        else
            Graphics_drawString(g_ctx,
                                (int8_t *) (term_buf + (first_line + i - lcd_lines)*(lcd_cols + 1)),
                                AUTO_STRING_LENGTH,
                                LCD_TERM_MARGIN,
                                LCD_TERM_MARGIN + i*line_spacing,
                                OPAQUE_TEXT);
    }
}

/*
 * writes a null terminated string to the terminal buffer
 * and adds a new line character at the end
 *
 * string is trimmed if it is too long to fit in the display
 */
void _LCD_term_add_line (char* txt) 
{
    uint8_t len = strlen(txt);

    if (len > lcd_cols) 
    {
        len = lcd_cols;
        txt[len] = '\0';
    }

    current_line++;

    if (current_line > lcd_lines) 
    {
        current_line = lcd_lines;
        first_line++;
        if (first_line == lcd_lines) first_line = 0;
    }

    if (first_line + (current_line - 1) < lcd_lines)
        strcpy(term_buf + (first_line + current_line - 1)*(lcd_cols + 1), txt);
    else
        strcpy(term_buf + (first_line + current_line - 1 - lcd_lines)*(lcd_cols + 1), txt);
}

/*
 * - allocates buffer memory
 * - stores dimensions (in characters) of the Graphics Context
 * - stores Context pointer
 *
 * returns:
 * 0  - success
 * 1  - terminal already initialized
 * -1 - memory allocation failed
 */
int LCD_term_init (Graphics_Context* ctx)
{
    static uint8_t first = 1;

    if (first) 
    {
        int LCD_h, LCD_w, font_w;

        LCD_h = Graphics_getDisplayHeight(ctx);
        LCD_w = Graphics_getDisplayWidth(ctx);

        line_spacing = Graphics_getFontHeight(&g_sFontFixed6x8) + 2;
        font_w = Graphics_getFontMaxWidth(&g_sFontFixed6x8);

        lcd_lines = (LCD_h - 2*LCD_TERM_MARGIN)/line_spacing;
        lcd_cols = (LCD_w - 2*LCD_TERM_MARGIN)/font_w;

        term_buf_size = lcd_lines * (lcd_cols+1);
        term_buf = malloc(term_buf_size);

        g_ctx = ctx;

        if (term_buf == NULL) return -1;
        else
        {
            first = 0;
            first_line = 0;
            current_line = 0;

            return 0;
        }
    }
    else return 1;
}

/*
 * prints txt on the terminal
 *
 * returns:
 * 0  - success
 * -1  - terminal not initialized
 * -2  - malloc failed
 */
int LCD_term_print (char* txt) 
{
    if (term_buf == NULL) return -1;

    /* allocate memory for a temporary string */
    int len = strlen(txt) + 1;
    char *txt_tmp = malloc(len);
    if (txt_tmp == NULL) return -2;

    strcpy(txt_tmp, txt);

    /* add to buffer line by line */
    char *pch;
    pch = strtok(txt_tmp, "\n");
    _LCD_term_add_line(pch);

    while (pch = strtok(NULL, "\n")) 
    {
        _LCD_term_add_line(pch);
    }

    _LCD_term_print_buf();

    free(txt_tmp);

    return 0;
}
