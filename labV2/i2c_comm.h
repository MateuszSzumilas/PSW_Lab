#ifndef		I2C_COMM_H
#define		I2C_COMM_H

/* ---------------------------
		ADuC834 I2C module
   --------------------------- */

#include <intx_t.h>

enum i2c_status {
	I2C_OK,
	I2C_NACK
};

/* arguments used in tx/rx functions:
		addr 	- slave address
		sub		- 8-bit sub-address
		*dat	- pointer to an array of transmitted/received data
		ndat	- number of bytes to send/read
*/
uint8_t I2C_TxData (uint8_t addr, uint8_t sub, uint8_t* dat, uint8_t ndat);
uint8_t I2C_RxData (uint8_t addr, uint8_t sub, uint8_t* dat, uint8_t ndat);

/* initialization of the I2C interface - device specific */
void I2C_Init();

/* ----------------------------*/

#endif		//I2C_COMM_H