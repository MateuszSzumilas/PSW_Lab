#include <aduc834.h>
#include <i2c_comm.h>
#include <intrins.h>

#define		I2C_RD		1
#define		I2C_WR		0
#define 	_I2C_PULSE() 	{MCO = 1; _nop_(); _nop_(); _nop_(); MCO = 0;}
#define		_I2C_IDLE() 	{MDE = 1; MDO = 1; MCO = 1;}

void I2C_Start ()
{
	MDE = 1;  // tx mode
	MDO = 1;
	MCO = 1;
	_nop_();
	_nop_();	
	MDO = 0;
	_nop_();
	_nop_();
	MCO = 0;
}

void I2C_Stop ()
{
	MDE = 1;  // tx mode
	MDO = 0;
	_nop_();
	_nop_();	
	MCO = 1;
	_nop_();
	_nop_();	
	MDO = 1;
	_nop_();
	_nop_();	
}

void I2C_Addr (uint8_t addr, uint8_t rw)
{
	uint8_t cnt;
	
	MDE = 1;  // tx mode
	
	addr = addr << 1;
	addr |= rw;
	
	for (cnt = 0; cnt < 8; cnt++)
	{
		MDO = addr & 0x80;
		addr = _crol_(addr,1);
		_I2C_PULSE();
	}	
	// data hold
	_nop_();
	_nop_();
}

uint8_t I2C_TxData (uint8_t addr, uint8_t sub, uint8_t* dat, uint8_t ndat)
{
	uint8_t cnt, cnt2;
	
	I2C_Start();
	I2C_Addr(addr, I2C_WR);
	
	// ack
	MDE = 0;  // rx mode	
	_I2C_PULSE();
	if (MDI) { _I2C_IDLE(); return I2C_NACK; } // MDI should be == 0 - ack received
	
	MDE = 1;  // tx mode
	// sub addr
	for (cnt = 0; cnt < 8; cnt++)
	{
		MDO = sub & 0x80;
		sub = _crol_(sub,1);
		_I2C_PULSE();
	}	
	// data hold
	_nop_();
	_nop_();
	
	// ack
	MDE = 0;  // rx mode	
	_I2C_PULSE();
	if (MDI) { _I2C_IDLE(); return I2C_NACK; } // MDI should be == 0 - ack received
	
	// transmit data
	for (cnt2 = 0; cnt2 < ndat; cnt2++)
	{
		uint8_t tmp = dat[cnt2];
		
		MDE = 1;  // tx mode
		// sub addr
		for (cnt = 0; cnt < 8; cnt++)
		{
			MDO = tmp & 0x80;
			tmp = _crol_(tmp,1);
			_I2C_PULSE();
		}
		// data hold
		_nop_();
		_nop_();
		
		// ack
		MDE = 0;  // rx mode	
		_I2C_PULSE();
		if (MDI) { _I2C_IDLE(); return I2C_NACK; } // MDI should be == 0 - ack received
	}
	
	I2C_Stop();
	
	return I2C_OK;
}

uint8_t I2C_RxData (uint8_t addr, uint8_t sub, uint8_t* dat, uint8_t ndat)
{
	uint8_t cnt, cnt2;
	
	I2C_Start();
	I2C_Addr(addr, I2C_WR);
	
	// ack
	MDE = 0;  // rx mode	
	_I2C_PULSE();
	if (MDI) { _I2C_IDLE(); return I2C_NACK; } // MDI should be == 0 - ack received
	
	MDE = 1;  // tx mode
	// sub addr
	for (cnt = 0; cnt < 8; cnt++)
	{
		MDO = sub & 0x80;
		sub = _crol_(sub,1);
		_I2C_PULSE();
	}	
	MDO = 1;
	// data hold
	_nop_();
	_nop_();
	
	// ack
	MDE = 0;  // rx mode	
	_I2C_PULSE();
	if (MDI) { _I2C_IDLE(); return I2C_NACK; } // MDI should be == 0 - ack received
	
	// repeated start
	I2C_Start();
	
	I2C_Addr(addr, I2C_RD);
	
	// ack
	MDE = 0;  // rx mode	
	_I2C_PULSE();
	if (MDI) { _I2C_IDLE(); return I2C_NACK; } // MDI should be == 0 - ack received
	
	// receive data
	for (cnt2 = 0; cnt2 < ndat; cnt2++)
	{
		uint8_t tmp = 0;
		
		MDE = 0;  // rx mode
		// sub addr
		for (cnt = 0; cnt < 8; cnt++)
		{
			_I2C_PULSE();
			tmp = _crol_(tmp,1);
			tmp |= MDI;
		}	
		// data hold
		_nop_();
		_nop_();
		
		dat[cnt2] = tmp;
		
		if (cnt2 < ndat-1)
		{
			// master ack
			MDE = 1;  // tx mode
			MDO = 0;
			_nop_();
			_I2C_PULSE();
		}
	}
	
	// master nack
	MDE = 1;  // tx mode
	MDO = 1;
	_nop_();
	_I2C_PULSE();
	
	I2C_Stop();
	
	return I2C_OK;
}

void i2c_init ()
{
	SPICON = 0x00;
	I2CCON = 0x08;

	_I2C_IDLE();
}