$NOMOD51				; Wy��czenie symboli standardowego 8051
						; Pozostaj� ACC, B, DPL, DPH, PSW, SP
$include (aduc834.h)	; W��czenie symboli dla ADuC834
; ______________________________________________________________________
; Deklaracja w�asnych oznacze�
FAN EQU P1.0			; Port steruj�cy prac� wentylatora
						; Je�li potrzebujesz, dopisz wi�cej deklaracji
; ______________________________________________________________________
CSEG					; Segment programu

ORG 0000H				; Adres w pami�ci programu 0000h
	JMP START			; Skocz do adresu przypisanego etykiecie START (0060h)

ORG 0060H 				; Tu zaczyna si� program g��wny
START:
; Od tego miejsca do etykiety LOOP znajduj� si� instrukcje wykonywane TYLKO jeden raz
; na pocz�tku programu. S� to instrukcje zwi�zane z inicjalizacj� uk�adu.
	MOV SP,#7FH			; Ustawienie wska�nika stosu na 7Fh
	MOV P0,#0			; Od��czenie zasilania silnik�w
	CLR FAN				; Wy��czenie wentylatora
	
; Od tego miejsca powinien rozpoczyna� si� kod programu g��wnego (wraz z wywo�aniem podprogram�w).

LOOP:
; Instrukcje zapisane pomi�dzy etykiet� LOOP a instrukcj� JMP LOOP, s� powtarzane
; do wy��czenia zasilania / do sygna�u reset i stanowi� p�tl� g��wn� programu.
	JMP LOOP
;______________________________________________________________________
; Tu powinien znajdowa� si� kod podprogram�w i obs�ugi przerwa� np.:
CZEKAJ: 
	NOP
	RET
; ______________________________________________________________________
END						; Koniec kodu do kompilacji